<?php
/**
 * Created by PhpStorm.
 * User: salt
 * Date: 29/10/18
 * Time: 18:47
 */

namespace App\Http\Controllers\Article;

use App\Http\Controllers\Controller;
use App\Article;

class BaseController extends Controller
{
    public function getArticleById($id)
    {
        $article = Article::getById($id);

        if (!$article->isEmpty()) {
            return $this->getSuccess('Data Article', $article);
        } else {
            return $this->getError('Data tidak ditemukan');
        }

    }

    public function checkArticle($id){
        $article = Article::getById($id);

        if ($article->isEmpty()) {
            return $this->getError('Data Article tidak ditemukan');
        }
    }

    public function getAllArticle($category, $tags, $publish, $highlight, $page)
    {
        $categoryArticle = null;
        if ($category) {
            $categoryArticle = $category;
        }

        $categoryTags = null;
        if ($tags) {
            $categoryTags = explode(',', $tags);
        }

        $getData = Article::getAll($categoryArticle, $categoryTags, $highlight, $publish, $page);

        if (!$getData->isEmpty()) {
            return $this->getSuccess('Data Article', $getData);
        } else {
            return $this->getError('Data tidak ditemukan');
        }
    }
}
