<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    //
    /**
     * @var
     */
    public $code;
    /**
     * @var
     */
    public $message;
    /**
     * @var
     */
    public $data;

    public function getError($message = null, $data = null)
    {
        $this->setCode(env('STATUS_FAILED_CODE'));
        if ($data != null) {
            $this->setData($data);
        }

        if ($message != null) {
            $this->setMessage($message);
        }
        return $this->getReturn();
    }

    /**
     * @param $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @param $data
     */
    public function setData($data = null)
    {
        $this->data = $data;
    }

    /**
     * @param $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function getReturn()
    {
        $return['code'] = $this->code;
        $return['message'] = $this->message;
        $return['data'] = $this->data;
        return $return;
    }

    public function getSuccess($message = null, $data = null)
    {
        $this->setCode(env('STATUS_SUCCESS_CODE'));
        if ($data != null) {
            $this->setData($data);
        }

        if ($message != null) {
            $this->setMessage($message);
        }
        return $this->getReturn();
    }
}
