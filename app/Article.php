<?php
/**
 * Created by PhpStorm.
 * User: Male
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Article extends Model
{
    protected $table = 'article';


    public static function getById($id)
    {

        $article = self::where('id', $id)->get();

        return $article;
    }

    public static function getAll($categoryid, $tags, $highlight, $publish, $page)
    {
        $newPage = $page - 1;

        $take = 5;
        $skip = 5;

        $article = self::when($categoryid, function ($article, $categoryid) {
            return $article->where('category_id', (int)$categoryid);
        })
            ->when($tags, function ($article, $tags) {
                for ($i = 0; $i < count($tags); $i++) {
                    $article->where('tags', 'like', '%' . $tags[$i] . '%');
                }
            })
            ->when($highlight, function ($article, $highlight) {
                return $article->where('isHighlight', $highlight);
            })
            ->when($publish, function ($article, $publish) {
                return $article->where('isPublished', $publish);
            })
            ->take(5)
            ->skip($skip + (($newPage - 1) * $take))->get();

        return $article;
    }

    function insertArticle($title, $slug, $shortdesc, $content, $categoryId, $tags, $author)
    {
        $time = \Carbon\Carbon::parse(\Carbon\Carbon::now());

        self::insert([
            [
                'title' => $title,
                'slug' => $slug,
                'author' => $author,
                'short_desc' => $shortdesc,
                'content' => $content,
                'category_id' => $categoryId,
                'tags' => $tags,
                'isPublished' => 0,
                'isHighlight' => 0,
                'created_date' => $time->timestamp,
            ]
        ]);

        return true;
    }

    public function updateArticle($id, $title, $slug, $shortdesc, $content, $categoryId, $tags, $author)
    {
        $time = \Carbon\Carbon::parse(\Carbon\Carbon::now());

        DB::table('article')->where('id', $id)
            ->update(
                [
                    'title' => $title,
                    'slug' => $slug,
                    'author' => $author,
                    'short_desc' => $shortdesc,
                    'content' => $content,
                    'category_id' => $categoryId,
                    'tags' => $tags,
                    'updated_date' => $time->timestamp,
                ]
            );

        return true;
    }

    public function highlightArticle($id, $flagHighlight)
    {
        $time = \Carbon\Carbon::parse(\Carbon\Carbon::now());
        DB::table('article')->where('id', $id)
            ->update(
                [
                    'isHighlight' => $flagHighlight,
                    'updated_date' => $time->timestamp
                ]
            );

        return true;
    }

    public function publishArticle($id, $flagPublish)
    {
        $time = \Carbon\Carbon::parse(\Carbon\Carbon::now());
        if ($flagPublish == 1) {
            DB::table('article')->where('id', $id)
                ->update(
                    [
                        'isPublished' => 1,
                        'publish_date' => $time->timestamp,
                        'updated_date' => $time->timestamp
                    ]
                );
        } else {
            DB::table('article')->where('id', $id)
                ->update(
                    [
                        'isPublished' => 1,
                        'updated_date' => $time->timestamp
                    ]
                );
        }

        return true;
    }

}