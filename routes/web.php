<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->get('/test', 'Article\ArticleController@testControl');

$router->get('/getarticlebyid', 'Article\ArticleController@getArticleFromId');

$router->post('/getbyid', 'Article\ArticleController@getArticleFromId');
$router->post('/getall', 'Article\ArticleController@getAll');

//post action
$router->post('/create-article', 'Article\ArticleController@createArticle');
$router->post('/update-article', 'Article\ArticleController@updateArticle');
$router->post('/publish-article', 'Article\ArticleController@publishArticle');
$router->post('/highlight-article', 'Article\ArticleController@highlightArticle');